<?php


namespace App\Models\Services\MessageBroker;


interface PublisherInterface
{
    public function publish(Message $message): void;
}
