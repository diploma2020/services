<?php


namespace App\Models\Zeebe;


use App\Models\Services\MessageBroker\Publishers\JobCompletePublisher;
use App\Models\Zeebe\JobType\TypeInterface;
use Illuminate\Database\Eloquent\Model;
use PhpAmqpLib\Wire\AMQPTable;

/**
 * Class Job
 * @package App\Models\Zeebe
 */
class Job extends Model
{
    protected $table = 'zeebe_jobs';

    protected $fillable = [
        'application_id', 'key', 'workflow_instance_key', 'job_type', 'variables'
    ];
}
