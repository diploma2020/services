@component('mail::message')
# Agency Agreement Offer

You have received this email due to your application for cooperation.

# Congratulations!
## Your application has reached the stage of signing the contract. Please read carefully all the files attached and, if you agree with all conditions,  click the button below to confirm and go to the next stage.


@component('mail::button', ['url' => $acceptUrl])
    I Accept The Offer Conditions
@endcomponent
Click <a href="{{$rejectUrl}}" style="color:red">there</a> for rejecting <b>application</b> right <b>now</b>
@endcomponent
