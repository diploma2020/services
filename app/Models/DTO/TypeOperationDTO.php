<?php


namespace App\Models\DTO;


class TypeOperationDTO extends AbstractDTO
{
    protected $jobKey;
    protected $workflowInstanceKey;
    protected $jobType;
    protected $variables;

    /**
     * @return mixed
     */
    public function getJobKey()
    {
        return $this->jobKey;
    }

    /**
     * @param mixed $jobKey
     */
    public function setJobKey($jobKey): void
    {
        $this->jobKey = $jobKey;
    }

    /**
     * @return mixed
     */
    public function getWorkflowInstanceKey()
    {
        return $this->workflowInstanceKey;
    }

    /**
     * @param mixed $workflowInstanceKey
     */
    public function setWorkflowInstanceKey($workflowInstanceKey): void
    {
        $this->workflowInstanceKey = $workflowInstanceKey;
    }

    /**
     * @return mixed
     */
    public function getJobType()
    {
        return $this->jobType;
    }

    /**
     * @param mixed $jobType
     */
    public function setJobType($jobType): void
    {
        $this->jobType = $jobType;
    }

    /**
     * @return mixed
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * @param mixed $variables
     */
    public function setVariables($variables): void
    {
        $this->variables = $variables;
    }

    public function putToVariables(string $key, $value)
    {
        $this->variables[$key] = $value;
    }

    public function getHeaders(): array
    {
        return [
            'XJobKey' => $this->jobKey,
            'XWorkflowInstanceKey' => $this->workflowInstanceKey
        ];
    }
}
