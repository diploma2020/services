<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['auth:api',], ], function () {
    Route::get('applications', 'ApplicationController@index');
    Route::post('applications', 'ApplicationController@store');
    Route::get('applications/{id}', 'ApplicationController@show');
    Route::get('applications/partner/application', 'ApplicationController@partnerApplication');
    Route::put('applications/{id}', 'ApplicationController@update');
    Route::delete('applications/{id}', 'ApplicationController@destroy');

    Route::put('applications/{id}/assign', 'ApplicationController@assign');
    Route::put('applications/{id}/unassign', 'ApplicationController@unassign');

    Route::put('applications/{id}/initial', 'ApplicationController@updateInitial');
    Route::put('applications/{id}/meeting', 'ApplicationController@updateMeeting');
    Route::put('applications/{id}/approve', 'ApplicationController@approve');
    Route::put('applications/{id}/reject', 'ApplicationController@reject');
});

Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');

Route::get('pub', 'RabbitController@pub');
Route::get('sub', 'RabbitController@sub');

Route::post('application/send', 'PartnerController@sendApplication');
Route::get('application/assign', 'PartnerController@assignApplication');
Route::get('application/complete', 'PartnerController@completeJob');

Route::get('offer/accept', 'PartnerController@offerAccept');
Route::get('account/accept', 'PartnerController@accountAccept');
