<?php

namespace App\Traits;

trait APIResponse
{
    public function sendResponse($message, $status, $success = false, $data = null)
    {
        return response()->json(
            [
                'status'    =>  $status,
                'success'   =>  $success,
                'message'   =>  $message,
                'data'      =>  $data
            ]
        );
    }
}
