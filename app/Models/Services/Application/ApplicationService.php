<?php

namespace App\Models\Services\Application;

use App\Models\Application;
use App\Models\Services\Auth\AuthService;
use App\Models\Services\MessageBroker\Publishers\JobCompletePublisher;
use App\Traits\APIResponse;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ApplicationService
{
    use APIResponse;

    const ALL_APPLICATIONS = 1;
    const MY_APPLICATIONS = 2;
    const UNASSIGNED_APPLICATIONS = 3;

    public function index($data)
    {
        if ($data['filter']==self::MY_APPLICATIONS) {
            $applications = Application::where('manager_id', $data['manager_id'])->orderBy('created_at', 'desc')->get();
        } elseif ($data['filter']==self::UNASSIGNED_APPLICATIONS) {
            $applications = Application::where('manager_id', null)->orderBy('created_at', 'desc')->get();
        } else {
            $applications = Application::orderBy('created_at', 'desc')->get();
        }

        return $this->sendResponse('', 200, true, ['applications' => $applications]);
    }

    public function store($data)
    {
        $application = Application::create([
            'uuid' => Str::uuid(),
            'partner_id' => $data['partner_id'],
            'bin' => $data['bin'],
            'company' => $data['company'],
            'address' => $data['address'],
        ]);

        if (!$application) {
            return $this->sendResponse('', 400, false);
        }

        $application = $application->fresh();

        $jobCompPublisher = new JobCompletePublisher();
        $jobCompPublisher->handle([
            'routing' => 'create-workflow-instance',
            'body' => [
                'uuid' => $application->uuid,
                'state' => 'initialization'
            ],
            'headers' => []
        ]);

        return $this->sendResponse('', 200, true);
    }

    public function show($data)
    {
        $application = Application::where('id', $data['id'])->first();

        if ($application) {
            return $this->sendResponse('', 200, true, ['application' => $application]);
        }

        return $this->sendResponse('', 400, false);
    }

    public function update($data)
    {
        $application = Application::where('id', $data['id'])->first();

        if (!$application) {
            return $this->sendResponse('', 400, false);
        }

        $application->update($data);
        $application = Application::where('id', $data['id'])->first();
        if ($application->state !== Application::NEW) {
            $applicationUpdatedService = new ApplicationStateUpdatedService();
            $applicationUpdatedService->handle($application);
        }

        return $this->sendResponse('', 200, true, ['application' => $application]);
    }

    public function destroy($data)
    {
        $application = Application::where('id', $data['id'])->first();

        if ($application) {
            $application->delete();
            return $this->sendResponse('', 200, true);
        }

        return $this->sendResponse('', 400, false);
    }

    public function partnerApplication($user)
    {
        $partnerId = $user->id;
        $is_active = $user->is_active;
        $application = Application::where('partner_id', $partnerId)->first();

        if ($application) {
            return $this->sendResponse('', 200, true, ['application' => $application, 'is_active' => $is_active]);
        }

        return $this->sendResponse('', 200, true, ['application' => null, 'is_active' => $is_active]);
    }
}
