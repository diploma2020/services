<?php


namespace App\Models\Zeebe\JobType;


use App\Models\DTO\DTOInterface;
use App\Models\DTO\JobCompleteDTO;
use App\Models\DTO\TypeOperationDTO;
use App\Models\Services\Zeebe\JobCompleteService;

/**
 * Class TypeAbstract
 * @package App\Models\Zeebe\JobType
 */
abstract class TypeAbstract implements TypeInterface
{
    const TYPE_INITIALIZATION           =   'initialization';
    const TYPE_REVIEW_APP               =   'review-app';
    const TYPE_MEETING                  =   'meeting';
    const TYPE_PREPARE_OFFER            =   'prepare-offer';
    const TYPE_RETRY_OFFER_SUBMIT       =   'retry-offer-submit';
    const TYPE_OFFER_RESPONSE           =   'offer-response';
    const TYPE_CALL_PARTNER_OFFER       =   'call-partner-offer';
    const TYPE_OFFER_CALL_RESPONSE      =   'offer-call-response';
    const TYPE_CREATE_ACC               =   'create-acc';
    const TYPE_RETRY_ACCOUNT_SUBMIT     =   'retry-account-submit';
    const TYPE_ACC_RESPONSE             =   'acc-response';
    const TYPE_CALL_PARTNER_ACC         =   'call-partner-acc';
    const TYPE_ACC_CALL_RESPONSE        =   'acc-call-response';
    const TYPE_REJECT_APP               =   'reject-app';
    const TYPE_MESSAGING                =   'message-send';

    const TYPES = [
        self::TYPE_INITIALIZATION,
        self::TYPE_REVIEW_APP,
        self::TYPE_MEETING,
        self::TYPE_PREPARE_OFFER,
        self::TYPE_RETRY_OFFER_SUBMIT,
        self::TYPE_OFFER_RESPONSE,
        self::TYPE_CALL_PARTNER_OFFER,
        self::TYPE_OFFER_CALL_RESPONSE,
        self::TYPE_CREATE_ACC,
        self::TYPE_RETRY_ACCOUNT_SUBMIT,
        self::TYPE_ACC_RESPONSE,
        self::TYPE_CALL_PARTNER_ACC,
        self::TYPE_ACC_CALL_RESPONSE,
        self::TYPE_REJECT_APP,
        self::TYPE_MESSAGING
    ];

    /** @var DTOInterface */
    protected $property;

    public function __construct(DTOInterface $property)
    {
        $this->property = $property;
    }

    public abstract function operation(): void;

    protected function completeJob(TypeOperationDTO $typeOperationDTO)
    {
        $service = new JobCompleteService();

        $jobCompleteDTO = new JobCompleteDTO();
        $jobCompleteDTO->setJobKey($typeOperationDTO->getJobKey());
        $jobCompleteDTO->setWorkflowInstanceKey($typeOperationDTO->getWorkflowInstanceKey());
        $jobCompleteDTO->setJobType($typeOperationDTO->getJobType());
        $jobCompleteDTO->setVariables($typeOperationDTO->getVariables());

        $service->handle($jobCompleteDTO);
    }
}
