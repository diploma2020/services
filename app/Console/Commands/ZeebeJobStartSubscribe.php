<?php

namespace App\Console\Commands;

use App\Models\Services\MessageBroker\Subscribers\ZeebeJobStartSubscriber;
use Illuminate\Console\Command;

class ZeebeJobStartSubscribe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zeebe:job-start-subscribe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Listens Zeebe jobs starts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new ZeebeJobStartSubscriber())->subscribe();

        return "subscriber start";
    }
}
