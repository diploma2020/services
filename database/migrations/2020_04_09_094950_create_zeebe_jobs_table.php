<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZeebeJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zeebe_jobs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('application_id');
            $table->unsignedBigInteger('key')->index();
            $table->unsignedBigInteger('workflow_instance_key')->index();
            $table->string('job_type');
            $table->json('variables')->nullable();
            $table->timestamps();
            $table->foreign('application_id')
                ->references('id')
                ->on('applications')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zeebe_jobs');
    }
}
