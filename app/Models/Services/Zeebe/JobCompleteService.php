<?php


namespace App\Models\Services\Zeebe;


use App\Models\DTO\JobCompleteDTO;
use App\Models\Services\AbstractService;
use App\Models\Services\MessageBroker\Publishers\JobCompletePublisher;

/**
 * Class JobCompleteService
 * @package App\Models\Services\Zeebe
 */
class JobCompleteService extends AbstractService
{
    /**
     * @param JobCompleteDTO $jobCompleteDTO
     */
    public function handle(JobCompleteDTO $jobCompleteDTO): void
    {
        $publisher = new JobCompletePublisher();
        $publisher->handle([
            'routing' => $jobCompleteDTO->getJobType(),
            'headers' => $jobCompleteDTO->getHeaders(),
            'body'    => $jobCompleteDTO->getVariables()
        ]);
    }
}
