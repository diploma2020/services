<?php


namespace App\Http\Controllers;


use App\Library\Rabbit;

class RabbitController
{
    /**
     * @var Rabbit
     */
    private $rabbit;

    public function __construct(Rabbit $rabbit)
    {
        $this->rabbit = $rabbit;
    }

    public function pub()
    {
        $routing = 'example.routing';
        $data = ['title' => 'qwerty123'];
        $exchange = 'example.exchange';

        $this->rabbit->pub($routing, $data, $exchange);
        return response(null, 200);
    }

    public function sub()
    {
        $queue = 'example.queue';

        $this->rabbit->sub($queue);
        return response(null, 200);
    }

}
