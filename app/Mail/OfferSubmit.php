<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OfferSubmit extends Mailable
{
    use Queueable, SerializesModels;

    public $acceptUrl;
    public $rejectUrl;

    protected $uuid;

    /**
     * Create a new message instance.
     *
     * @param string $uuid
     */
    public function __construct(string $uuid)
    {
        $this->uuid = $uuid;
        $this->acceptUrl = $this->getHost() . '/api/offer/accept?uuid='.$uuid;
        $this->rejectUrl = $this->getHost() . '/api/offer/accept?state=reject&uuid='.$uuid;
    }

    private function getHost()
    {
        return config('services.self');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to('sedi.dev@gmail.com')
            ->subject('Offer for agency agreement')
            ->markdown('emails.offer')
            ->attach(storage_path('app/public/applications/offers/'.$this->uuid.'.pdf'), ['as' => "agreement.pdf"]);;
    }
}

