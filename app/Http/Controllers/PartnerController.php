<?php


namespace App\Http\Controllers;


use App\Library\Rabbit;
use App\Models\Application;
use App\Models\DTO\JobCompleteDTO;
use App\Models\Services\Application\ApplicationService;
use App\Models\Services\MessageBroker\Publishers\JobCompletePublisher;
use App\Models\Services\Zeebe\JobCompleteService;
use App\Models\Zeebe\Job;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use PhpAmqpLib\Wire\AMQPTable;
use Ramsey\Uuid\Uuid;

class PartnerController extends Controller
{
    /**
     * @var Rabbit
     */
    private $rabbit;

    protected $exchange = 'zeebe.job';

    public function __construct(Rabbit $rabbit)
    {
        $this->rabbit = $rabbit;
    }

    public function sendApplication(Request $request)
    {
//        $routing = 'zeebe.job.complete.create-workflow-instance';
//        $data = [
//            'title' => 'qwerty123',
//            'uuid'  => Uuid::uuid4()->toString(),
//            'state' => 'active'
//        ];
//
//
//        $this->rabbit->pub($routing, $data, $this->exchange, new AMQPTable());

        $user = new User([
            'name' => $request->post('name'),
            'email' => $request->post('email'),
            'is_partner' => true,
            'password' => 'pass'
        ]);

        if (!$user->save()) {
            throw new \Exception('User not created');
        }

        $application = new Application([
            'uuid' => Str::uuid(),
            'company' => $request->post('company'),
            'partner_id' => $user->id
        ]);

        if (!$application->save()) {
            throw new \Exception('Application not created');
        }
        $application = $application->fresh();

//        return $application->partner->name;
        $jobCompPublisher = new JobCompletePublisher();
        $jobCompPublisher->handle([
            'routing' => 'create-workflow-instance',
            'body' => [
                'uuid' => $application->uuid,
                'state' => 'initialization'
//                'state' => $application->state
            ],
            'headers' => []
        ]);
//
        return response('Success');
    }

    public function assignApplication(Request $request)
    {
        $jobKey = intval($request->get('job_key'));
        $workflowInstanceKey = intval($request->get('workflow_instance_key'));

        $headers = [
            'XJobKey' => $jobKey,
            'XWorkflowInstanceKey' => $workflowInstanceKey
        ];

        $header = new AMQPTable();
        $header->set('XJobKey', $jobKey);
        $header->set('XWorkflowInstanceKey', $workflowInstanceKey);

        $routing = 'zeebe.job.complete.initialization';

        $this->rabbit->pub($routing, ["some_data" => "Asdcas"], $this->exchange, $header);
        return response(null, 200);
    }

    public function completeJob(Request $request)
    {
        $jobKey = intval($request->get('job_key'));
        $jobType = $request->get('job_type');

        /** @var Job $job */
        $job = Job::where('key', $jobKey)->get()->last();

        $service = new JobCompleteService();

        $jobCompleteDTO = new JobCompleteDTO();
        $jobCompleteDTO->setJobKey($job->getAttribute('key'));
        $jobCompleteDTO->setWorkflowInstanceKey($job->getAttribute('workflow_instance_key'));
        $jobCompleteDTO->setJobType($jobType);
        $var = json_decode($job->getAttribute('variables'), true);

        $var['state'] = 'rejected';
//        return $var;
//        $jobCompleteDTO->setVariables(json_decode($job->getAttribute('variables'), true));
        $jobCompleteDTO->setVariables($var);

        $service->handle($jobCompleteDTO);

        return response('OK');
    }

    public function offerAccept(Request $request, JobCompleteService $service, ApplicationService $applicationService)
    {
        $uuid = $request->get('uuid');
        $state = $request->get('state');

        /** @var Application $application */
        $application = Application::where('uuid', $uuid)->firstOrFail();

        $data = [
          'id' => $application->id,
          'state' => Application::ACCOUNT
        ];

        if ($state and $state === 'reject') {
            $data['state'] = Application::REJECT;
        }

        $applicationService->update($data);

        /** @var Job $job */
        $job = $application->jobs()->orderBy('created_at', 'desc')->first();

        $jobCompleteDTO = new JobCompleteDTO();
        $jobCompleteDTO->setJobKey($job->getAttribute('key'));
        $jobCompleteDTO->setWorkflowInstanceKey($job->getAttribute('workflow_instance_key'));
        $jobCompleteDTO->setJobType('offer-response');
        $vars = json_decode($job->getAttribute('variables'), true);
        if ($state and $state === 'reject') {
            $vars['state'] = 'rejected';
        }
        $jobCompleteDTO->setVariables($vars);

        $service->handle($jobCompleteDTO);

        return Redirect::to('http://localhost:8000/admin/applications/'.$application->id.'/edit');
    }

    public function accountAccept(Request $request, JobCompleteService $service, ApplicationService $applicationService)
    {
        $uuid = $request->get('uuid');

        /** @var Application $application */
        $application = Application::getByUuidOrFail($uuid);

        $data = [
            'id' => $application->id,
            'state' => Application::APPROVE
        ];

        $applicationService->update($data);

        /** @var Job $job */
        $job = $application->jobs()->orderBy('created_at', 'desc')->first();

        $jobCompleteDTO = new JobCompleteDTO();
        $jobCompleteDTO->setJobKey($job->getAttribute('key'));
        $jobCompleteDTO->setWorkflowInstanceKey($job->getAttribute('workflow_instance_key'));
        $jobCompleteDTO->setJobType('acc-response');
        $vars = json_decode($job->getAttribute('variables'), true);
        $vars['state'] = 'approved';
        $jobCompleteDTO->setVariables($vars);

        $service->handle($jobCompleteDTO);

        return Redirect::to('http://localhost:8000/admin/content');
    }
}
