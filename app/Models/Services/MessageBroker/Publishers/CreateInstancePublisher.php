<?php


namespace App\Models\Services\MessageBroker\Publishers;


use App\Models\Services\MessageBroker\Message;
use App\Models\Services\MessageBroker\Publisher;
use PhpAmqpLib\Wire\AMQPTable;

class CreateInstancePublisher extends Publisher
{
    const EXCHANGE = 'zeebe.job';
    const BASE_ROUTING = 'zeebe.job.complete.';

    /** @var string */
    private $routing;

    public function getExchange(): string
    {
        return self::EXCHANGE;
    }

    public function getRouting(): string
    {
        return self::BASE_ROUTING . $this->routing;
    }

    public function handle(array $data)
    {
        $this->routing = $data['routing'];
        $message = new Message();
        $message->setBody($data['body']);

        $headers = new AMQPTable();
        foreach ($data['headers'] as $key => $value) {
            $headers->set($key, $value);
        }

        $message->setHeaders($headers);
        $this->publish($message);
    }

    public function publish(Message $message): void
    {
        parent::publish($message);
    }
}
