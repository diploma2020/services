<?php


namespace App\Models\Services\Mail;


use App\Mail\ApplicationRejected;
use App\Models\Application;
use Illuminate\Support\Facades\Mail;

class RejectApplication
{
    public static function submit(string $uuid)
    {
        $to = Application::getByUuidOrFail($uuid)->partner->email;
        Mail::to('sedi.dev@gmail.com')
            ->send(new ApplicationRejected());

        Mail::send(new ApplicationRejected());
    }
}
