<?php


namespace App\Library;


use Amqp;
use Bschmitt\Amqp\Message;

class Rabbit
{
    public function pub($routing, $data, $exchange, $headers = 'table_object')
    {
        $properties = [
            'application_headers' => $headers,
            'content_type' => 'application/json',
        ];
        $message = new Message(json_encode($data), $properties);
        Amqp::publish($routing, $message, ['exchange' => $exchange]);
    }

    public function sub(string $queue)
    {
        Amqp::consume($queue, function ($message, $resolver){
            $data = json_decode($message->body, true);
            var_dump($data);
            $resolver->acknowledge($message);

            // TODO можно убрать, если работает на фоне
            $resolver->stopWhenProcessed();
        });
    }

}
