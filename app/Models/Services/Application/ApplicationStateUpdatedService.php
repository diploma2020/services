<?php


namespace App\Models\Services\Application;


use App\Models\Application;
use App\Models\DTO\JobCompleteDTO;
use App\Models\Services\Zeebe\JobCompleteService;
use App\Models\Zeebe\Job;

class ApplicationStateUpdatedService
{
    public function handle(Application $application): void
    {
        $jobCompleteService = new JobCompleteService();
        /** @var Job $job */
        $job = $application->jobs()->orderBy('created_at', 'desc')->first();


        if (!$job) {
            throw new \Exception('Job not found');
        }

        $jobCompleteDTO = new JobCompleteDTO();
        $jobCompleteDTO->setJobKey($job->getAttribute('key'));
        $jobCompleteDTO->setWorkflowInstanceKey($job->getAttribute('workflow_instance_key'));
        $jobCompleteDTO->setJobType($job->getAttribute('job_type'));
        $vars = json_decode($job->getAttribute('variables'), true);
        $vars['state'] = $this->getState($application->state);
        $jobCompleteDTO->setVariables($vars);

        $jobCompleteService->handle($jobCompleteDTO);
    }

    private function getState(int $state): string
    {
        switch ($state) {
            case Application::MEET:
                return 'meeting';
                break;
            case Application::OFFER:
                return 'offer';
                break;
            case Application::ACCOUNT:
                return 'account';
                break;
            case Application::REJECT:
                return 'rejected';
                break;
            case Application::APPROVE:
                return 'approved';
                break;
            default:
                return 'error';
        }
    }
}
