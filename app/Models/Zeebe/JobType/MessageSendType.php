<?php


namespace App\Models\Zeebe\JobType;


use App\Mail\AccountInfoSend;
use App\Mail\ApplicationRejected;
use App\Mail\OfferSubmit;
use App\Models\DTO\TypeOperationDTO;
use Illuminate\Support\Facades\Mail;

class MessageSendType extends TypeAbstract
{
    public function operation(): void
    {
        /** @var TypeOperationDTO $typeOperation */
        $typeOperation = $this->property;
        var_dump((array) $this->property);

//        (new CreateJobIfNotExists())->handle($typeOperation);

        $this->sendMessage($typeOperation->getVariables()['state'], $typeOperation->getVariables()['uuid']);
        $this->completeJob($typeOperation);
    }

    public function sendMessage(string $state, string $uuid)
    {
        switch ($state) {
            case 'offer':
                Mail::send(new OfferSubmit($uuid));
                break;
            case 'account':
                Mail::send(new AccountInfoSend($uuid));
                break;
            case 'rejected':
                Mail::send(new ApplicationRejected());
                break;
            default:
                throw new \Exception('Wrong message state');
        }
    }
}
