<?php

namespace App\Models;

use App\User;
use App\Models\Zeebe\Job;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    const NEW = 1;
    const MEET = 2;
    const OFFER = 3;
    const ACCOUNT = 4;
    const REJECT = 5;
    const APPROVE = 6;

    protected $fillable = [
        'uuid', 'state', 'is_rejected', 'partner_id', 'manager_id', 'bin', 'company', 'address',
        'meeting', 'bank', 'payment_period', 'commission', 'min_cashback', 'max_cashback', 'discount',
        'payment_method'
    ];

    protected $with = [
        'partner', 'manager',
    ];

    protected $hidden = [
        'partner_id', 'manager_id',
    ];

    public function partner()
    {
        return $this->hasOne(User::class, 'id', 'partner_id');
    }

    public function manager()
    {
        return $this->hasOne(User::class, 'id', 'manager_id');
    }

    public function jobs()
    {
        return $this->hasMany(Job::class, 'application_id');
    }

    public static function getByUuidOrFail(string $uuid): self
    {
        return self::where('uuid', $uuid)->firstOrFail();
    }
}
