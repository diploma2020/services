<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Services\Application\ApplicationService;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    /**
     * @var ApplicationService
     */
    private $applicationService;

    public function __construct(ApplicationService $applicationService)
    {
        $this->applicationService = $applicationService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request)
    {
        $data = [
            'filter' => $request->get('filter'),
            'manager_id' => $request->user()->id,
        ];

        $response = $this->applicationService->index($data);
        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            'partner_id' => $request->user()->id,
            'bin' => $request->post('bin'),
            'company' => $request->post('company'),
            'address' => $request->post('address'),
        ];

        $response = $this->applicationService->store($data);
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @param Request $request
     * @return void
     */
    public function show($id, Request $request)
    {
        $data = [
            'id' => $id,
        ];

        $response = $this->applicationService->show($data);
        return $response;
    }

    public function partnerApplication(Request $request)
    {
        $user = $request->user();

        $response = $this->applicationService->partnerApplication($user);
        return $response;
    }

    public function update($id, Request $request)
    {
        $data = $request->all();
        $data['id'] = $id;

        $response = $this->applicationService->update($data);
        return $response;
    }

    public function updateInitial($id, Request $request)
    {
        $data = [
            'company' => $request->post('company'),
            'address' => $request->post('address'),
            'bin' => $request->post('bin')
        ];
        $data['id'] = $id;
        $data['state'] = 2;

        $response = $this->applicationService->update($data);
        return $response;
    }

    public function updateMeeting($id, Request $request)
    {
        $data = [
            'bank' => $request->post('bank'),
            'payment_period' => $request->post('payment_period'),
            'commission' => $request->post('commission'),
            'min_cashback' => $request->post('min_cashback'),
            'max_cashback' => $request->post('max_cashback'),
            'discount' => $request->post('discount')
        ];
        $data['id'] = $id;
        $data['state'] = 3;

        $response = $this->applicationService->update($data);
        return $response;
    }

    public function approve($id, Request $request)
    {
        $data['id'] = $id;
        $data['state'] = 4;

        $response = $this->applicationService->update($data);
        return $response;
    }

    public function reject($id, Request $request)
    {
        $data['id'] = $id;
        $data['is_rejected'] = true;
        $data['state'] = Application::REJECT;

        $response = $this->applicationService->update($data);
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $data = [
            'id' => $id,
        ];

        $response = $this->applicationService->destroy($data);
        return $response;
    }

    public function assign($id, Request $request)
    {
        $data = [
            'id' => $id,
            'manager_id' => $request->user()->id,
        ];

        $response = $this->applicationService->update($data);
        return $response;
    }

    public function unassign($id, Request $request)
    {
        $data = [
            'id' => $id,
            'manager_id' => null,
        ];

        $response = $this->applicationService->update($data);
        return $response;
    }
}
