<?php


namespace App\Models\Zeebe\JobType;


use App\Models\DTO\TypeOperationDTO;
use App\Models\Handlers\CreateJobIfNotExists;
use App\Models\Zeebe\Job;

class ReviewApplicationType extends TypeAbstract
{
    public function operation(): void
    {
        /** @var TypeOperationDTO $typeOperation */
        $typeOperation = $this->property;
        var_dump((array) $this->property);

        (new CreateJobIfNotExists())->handle($typeOperation);

//        $typeOperation->putToVariables('state', 'review');
//        $this->completeJob($typeOperation);
    }
}
