<?php

namespace App\Http\Controllers;

use App\Models\Services\Auth\AuthService;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    private $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function register(Request $request)
    {
        $data = [
            'email' => $request->post('email'),
            'password' => $request->post('password'),
            'name'  => $request->post('name'),
            'type' => $request->post('type'),
        ];

        $response = $this->authService->register($data);
        return $response;
    }

    public function login(Request $request)
    {
        $data = [
            'email' => $request->post('email'),
            'password' => $request->post('password'),
        ];

        $response = $this->authService->login($data);
        return $response;
    }
}
