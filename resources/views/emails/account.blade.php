@component('mail::message')
# Congratulations!!! From Diploma

## Your onboarding process has successfully completed
### GO through link below, and enjoy our platform ;)

@component('mail::button', ['url' => $url])
I am Partner 😎
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
