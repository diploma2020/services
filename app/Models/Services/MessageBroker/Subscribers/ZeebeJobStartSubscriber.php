<?php


namespace App\Models\Services\MessageBroker\Subscribers;

use Amqp;
use App\Models\DTO\TypeOperationDTO;
use App\Models\Services\MessageBroker\Subscriber;
use App\Models\Zeebe\JobType\TypeAbstract;
use App\Models\Zeebe\JobType\TypeFactory;
use Bschmitt\Amqp\Message;
use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Log;
use PhpAmqpLib\Wire\AMQPTable;

/**
 * Class ZeebeJobStartSubscriber
 * @package App\Models\Services\MessageBroker\Subscribers
 */
class ZeebeJobStartSubscriber extends Subscriber
{
    /**
     *
     */
    public function subscribe(): void
    {
        Amqp::consume('', function ($message, $resolver){
            $body = json_decode($message->body, true);
            $resolver->acknowledge($message);

            $data = [
                'routing_key' => $message->delivery_info['routing_key'],
                'headers' => $message->get_properties()['application_headers']->getNativeData(),
                'body' => $body
            ];

            Log::debug($data['routing_key']);
            foreach ($data['headers'] as $key => $value) {
                Log::debug('HEADER: ' . $key . ' => ' .$value);
            }
            foreach ($data['body'] as $key => $value) {
                Log::debug('BODY: ' . $key . ' => ' .$value);
            }

            $this->handle($data);

        }, [
            'exchange' => 'zeebe.job',
            'exchange_type' => 'topic',
            'routing'   => 'zeebe.job.start.#',
            'queue_force_declare' => true,
            'queue_exclusive' => true,
            'persistent' => true
        ]);
    }

    /**
     * @param array $data
     * @throws \Exception
     */
    public function handle(array $data): void
    {
        $jobTypeStr = end(explode('.', $data['routing_key']));

        $typeOperationDto = new TypeOperationDTO();
        $typeOperationDto->setJobType($jobTypeStr);
        $typeOperationDto->setJobKey($data['headers']['XJobKey']);
        $typeOperationDto->setWorkflowInstanceKey($data['headers']['XWorkflowInstanceKey']);
        $typeOperationDto->setVariables($data['body']);

        $typeFactory = new TypeFactory();
        $typeFactory->handle($jobTypeStr, $typeOperationDto)->operation();
    }
}
