<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE>Diploma Offer (1)</TITLE>
<META name="generator" content="BCL easyConverter SDK 5.0.210">
<STYLE type="text/css">

body {margin-top: 0px;margin-left: 0px;}

#page_1 {position:relative; overflow: hidden;margin: 123px 0px 321px 96px;padding: 0px;border: none;width: 699px;}





#page_2 {position:relative; overflow: hidden;margin: 125px 0px 137px 96px;padding: 0px;border: none;width: 699px;}





#page_3 {position:relative; overflow: hidden;margin: 125px 0px 334px 96px;padding: 0px;border: none;width: 699px;}





.ft0{font: 32px 'Arial';line-height: 36px;}
.ft1{font: italic 20px 'Arial';line-height: 24px;}
.ft2{font: 20px 'Arial';color: #ffffff;line-height: 23px;}
.ft3{font: 20px 'Arial';line-height: 29px;}
.ft4{font: bold 20px 'Arial';line-height: 24px;}
.ft5{font: 20px 'Arial';line-height: 28px;}
.ft6{font: 20px 'Arial';line-height: 23px;}
.ft7{font: 20px 'Arial';margin-left: 12px;line-height: 29px;}
.ft8{font: 20px 'Arial';margin-left: 12px;line-height: 23px;}
.ft9{font: 1px 'Arial';line-height: 7px;}

.p0{text-align: left;padding-left: 167px;margin-top: 0px;margin-bottom: 0px;}
.p1{text-align: left;padding-left: 45px;margin-top: 99px;margin-bottom: 0px;}
.p2{text-align: left;padding-left: 104px;margin-top: 3px;margin-bottom: 0px;}
.p3{text-align: left;padding-left: 202px;margin-top: 35px;margin-bottom: 0px;}
.p4{text-align: left;padding-left: 298px;margin-top: 34px;margin-bottom: 0px;}
.p5{text-align: left;padding-right: 133px;margin-top: 35px;margin-bottom: 0px;}
.p6{text-align: left;padding-left: 243px;margin-top: 28px;margin-bottom: 0px;}
.p7{text-align: left;padding-right: 98px;margin-top: 34px;margin-bottom: 0px;}
.p8{text-align: left;padding-right: 143px;margin-top: 28px;margin-bottom: 0px;}
.p9{text-align: left;padding-left: 188px;margin-top: 0px;margin-bottom: 0px;}
.p10{text-align: left;margin-top: 34px;margin-bottom: 0px;}
.p11{text-align: left;padding-left: 48px;padding-right: 116px;margin-top: 58px;margin-bottom: 0px;text-indent: -24px;}
.p12{text-align: left;margin-top: 49px;margin-bottom: 0px;}
.p13{text-align: left;padding-left: 48px;padding-right: 121px;margin-top: 58px;margin-bottom: 0px;text-indent: -24px;}
.p14{text-align: left;margin-top: 27px;margin-bottom: 0px;}
.p15{text-align: left;padding-left: 24px;margin-top: 59px;margin-bottom: 0px;}
.p16{text-align: left;padding-left: 48px;margin-top: 59px;margin-bottom: 0px;}
.p17{text-align: left;padding-right: 100px;margin-top: 34px;margin-bottom: 0px;}
.p18{text-align: left;padding-right: 107px;margin-top: 28px;margin-bottom: 0px;}
.p19{text-align: left;padding-left: 204px;margin-top: 0px;margin-bottom: 0px;}
.p20{text-align: left;padding-right: 113px;margin-top: 34px;margin-bottom: 0px;}
.p21{text-align: left;padding-left: 7px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p22{text-align: left;padding-left: 5px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p23{text-align: left;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p24{text-align: left;margin-top: 86px;margin-bottom: 0px;}

.td0{border-left: #000000 1px solid;border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 300px;vertical-align: bottom;}
.td1{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 299px;vertical-align: bottom;}
.td2{border-left: #000000 1px solid;border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 300px;vertical-align: bottom;}
.td3{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 299px;vertical-align: bottom;}
.td4{border-left: #000000 1px solid;border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 300px;vertical-align: bottom;}
.td5{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 299px;vertical-align: bottom;}
.td6{padding: 0px;margin: 0px;width: 301px;vertical-align: bottom;}
.td7{padding: 0px;margin: 0px;width: 201px;vertical-align: bottom;}

.tr0{height: 34px;}
.tr1{height: 7px;}
.tr2{height: 31px;}
.tr3{height: 35px;}
.tr4{height: 67px;}

.t0{width: 601px;margin-top: 55px;font: 20px 'Arial';}
.t1{width: 502px;margin-left: 29px;margin-top: 84px;font: 20px 'Arial';}

</STYLE>
</HEAD>

<BODY>
<DIV id="page_1">


<P class="p0 ft0">Agency agreement</P>
<P class="p1 ft1">For a contemplated Partnership between {{$company}}</P>
<P class="p2 ft1">and Diploma Inc. (or “Diploma”).</P>
<P class="p3 ft1">Submitted by: Diploma</P>
<P class="p4 ft2">.</P>
<P class="p5 ft3">This agreement contains the proprietary information of Diploma and shall not be shared outside of the above named parties.</P>
<P class="p6 ft4">Background.</P>
<P class="p7 ft3">The agreement was created automatically, after the application was left by {{$company}}, and subsequent meetings and agreements between {{$company}} and the employees of Diploma Inc.</P>
<P class="p8 ft5">The parties believe there may be mutual benefit in forming a business partnership and pooling their resources and assets in order to pursue the objectives below.</P>
</DIV>
<DIV id="page_2">


<P class="p9 ft4">Objectives of the parties.</P>
<P class="p10 ft6">Marketing Management –</P>
<P class="p11 ft3"><SPAN class="ft6">-</SPAN><SPAN class="ft7">A pool of marketing tools to attract new users into {{$company}}'s business, tracking their dynamics, and increasing the company's awareness.</SPAN></P>
<P class="p12 ft6">Loyalty Management –</P>
<P class="p13 ft3"><SPAN class="ft6">-</SPAN><SPAN class="ft7">A pool of loyalty instruments to increase of customers retention to {{$company}}'s business, tracking their dynamics.</SPAN></P>
<P class="p14 ft6">Data & Analytics –</P>
<P class="p15 ft6"><SPAN class="ft6">-</SPAN><SPAN class="ft8">A </SPAN><NOBR>web-based</NOBR> data and analytics platform</P>
<P class="p16 ft4">Contributions & responsibilities of the parties.</P>
<P class="p17 ft3">Diploma Inc. owns intellectual property, including, but not limited to, plans, methods and processes needed for the development of the systems and applications contemplated above.</P>
<P class="p18 ft5">{{$company}} may use the platform tools provided by Diploma Inc. in accordance with the access provided by them and within the framework of the following payment conditions.</P>
</DIV>
<DIV id="page_3">


<P class="p19 ft4">Legal considerations.</P>
<P class="p20 ft3">This agreement is based on the following key indicators, agreed in advance and described in the table below</P>
<TABLE cellpadding=0 cellspacing=0 class="t0">
<TR>
	<TD class="tr0 td0"><P class="p21 ft4">Name</P></TD>
	<TD class="tr0 td1"><P class="p22 ft4">Value</P></TD>
</TR>
<TR>
	<TD class="tr1 td2"><P class="p23 ft9">&nbsp;</P></TD>
	<TD class="tr1 td3"><P class="p23 ft9">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr2 td4"><P class="p21 ft6">Commission</P></TD>
	<TD class="tr2 td5"><P class="p22 ft6">{{$commission}} %</P></TD>
</TR>
<TR>
	<TD class="tr1 td2"><P class="p23 ft9">&nbsp;</P></TD>
	<TD class="tr1 td3"><P class="p23 ft9">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr2 td4"><P class="p21 ft6">Frequency of payments</P></TD>
	<TD class="tr2 td5"><P class="p22 ft6">{{$paymentFrequency}}</P></TD>
</TR>
<TR>
	<TD class="tr1 td2"><P class="p23 ft9">&nbsp;</P></TD>
	<TD class="tr1 td3"><P class="p23 ft9">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr2 td4"><P class="p21 ft6">Min and Max Cashback</P></TD>
	<TD class="tr2 td5"><P class="p22 ft6">from {{$minCashback}}% to {{$maxCashback}}%</P></TD>
</TR>
<TR>
	<TD class="tr1 td2"><P class="p23 ft9">&nbsp;</P></TD>
	<TD class="tr1 td3"><P class="p23 ft9">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr2 td4"><P class="p21 ft6">Discount</P></TD>
	<TD class="tr2 td5"><P class="p22 ft6">{{$discount}}</P></TD>
</TR>
<TR>
	<TD class="tr1 td2"><P class="p23 ft9">&nbsp;</P></TD>
	<TD class="tr1 td3"><P class="p23 ft9">&nbsp;</P></TD>
</TR>
</TABLE>
<P class="p24 ft6">AGREED BY THE PARTIES</P>
<TABLE cellpadding=0 cellspacing=0 class="t1">
<TR>
	<TD class="tr3 td6"><P class="p23 ft6">{{$directorName}}</P></TD>
	<TD class="tr3 td7"><P class="p23 ft6">Excellent Students</P></TD>
</TR>
<TR>
	<TD class="tr4 td6"><P class="p23 ft6">{{$company}} Director</P></TD>
	<TD class="tr4 td7"><P class="p23 ft6">Diploma Inc. President</P></TD>
</TR>
</TABLE>
</DIV>
</BODY>
</HTML>
