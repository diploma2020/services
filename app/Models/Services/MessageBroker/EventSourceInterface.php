<?php


namespace App\Models\Services\MessageBroker;


interface EventSourceInterface
{
    public function handle(array $data);
}
