<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AccountInfoSend extends Mailable
{
    use Queueable, SerializesModels;

    public $url;

    protected $uuid;

    /**
     * Create a new message instance.
     *
     * @param string $uuid
     */
    public function __construct(string $uuid)
    {
        $this->url = $this->getHost() . '/api/account/accept?uuid='.$uuid;
        $this->uuid = $uuid;
    }

    private function getHost()
    {
        return config('services.self');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to('sedi.dev@gmail.com')
            ->subject('End of Onboarding Process')
            ->markdown('emails.account');
    }
}
