<?php


namespace App\Models\Handlers;


use App\Models\Application;
use App\Models\DTO\TypeOperationDTO;
use App\Models\Zeebe\Job;

class CreateJobIfNotExists
{
    public function handle(TypeOperationDTO $typeOperation): ?Job
    {
        sleep(1);
        if (!Job::where('key', $typeOperation->getJobKey())->exists()) {
            return Job::create([
                'application_id' => Application::where('uuid', $typeOperation->getVariables()['uuid'])->first()->id,
                'key' => $typeOperation->getJobKey(),
                'workflow_instance_key' => $typeOperation->getWorkflowInstanceKey(),
                'job_type' => $typeOperation->getJobType(),
                'variables' => json_encode($typeOperation->getVariables(), true)
            ]);
        }

        return null;
    }
}
