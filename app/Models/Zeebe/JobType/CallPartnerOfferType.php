<?php


namespace App\Models\Zeebe\JobType;


use App\Models\Application;
use App\Models\DTO\TypeOperationDTO;
use App\Models\Handlers\CreateJobIfNotExists;
use App\Models\Services\Application\ApplicationService;
use App\Models\Zeebe\Job;

class CallPartnerOfferType extends TypeAbstract
{
    public function operation(): void
    {
        /** @var TypeOperationDTO $typeOperation */
        $typeOperation = $this->property;
        var_dump((array) $this->property);

        (new CreateJobIfNotExists())->handle($typeOperation);

//        $typeOperation->putToVariables('state', 'rejected');

        /** @var Application $application */
        $application = Application::where('uuid', $typeOperation->getVariables()['uuid'])->firstOrFail();

        $data = [
            'id' => $application->id,
            'state' => Application::REJECT,
            'is_rejected' => true
        ];

        $applicationService = new ApplicationService();
        $applicationService->update($data);

//        $this->completeJob($typeOperation);
    }
}
