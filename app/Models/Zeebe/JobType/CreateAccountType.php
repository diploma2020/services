<?php


namespace App\Models\Zeebe\JobType;


use App\Models\Application;
use App\Models\DTO\TypeOperationDTO;
use App\Models\Handlers\CreateJobIfNotExists;
use App\Models\Zeebe\Job;
use App\User;

class CreateAccountType extends TypeAbstract
{
    public function operation(): void
    {
        /** @var TypeOperationDTO $typeOperation */
        $typeOperation = $this->property;
//        var_dump((array) $this->property);

        (new CreateJobIfNotExists())->handle($typeOperation);
        $typeOperation->putToVariables('state', 'account');
        $uuid = $typeOperation->getVariables()['uuid'];

        $application = Application::getByUuidOrFail($uuid);

        /** @var User $partner */
        $partner = $application->partner;

        $partner->update([
           'is_active' => true
        ]);

        $this->completeJob($typeOperation);
    }
}
