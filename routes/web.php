<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/offer', function () {
    return view('diplomaOffer');
});

Route::get('/offer/accept', function () {
    return (new \App\Mail\OfferSubmit("41332a3b-f5da-46d9-ad81-cb23653b26cb"))->render();
});

Route::get('/account/accept', function () {
    return (new \App\Mail\AccountInfoSend("41332a3b-f5da-46d9-ad81-cb23653b26cb"))->render();
});

Route::get('/offer/print', 'PdfController@printPdf');
