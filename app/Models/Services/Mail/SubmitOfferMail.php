<?php


namespace App\Models\Services\Mail;


use App\Mail\OfferSubmit;
use App\Models\Application;
use Illuminate\Support\Facades\Mail;
use Barryvdh\DomPDF\Facade as PDF;

class SubmitOfferMail
{
    public static function submit($uuid)
    {
        $to = Application::getByUuidOrFail($uuid)->partner->email;
        Mail::to('sedi.dev@gmail.com')
            ->send(new OfferSubmit($uuid));
//        Mail::send('offer', [], function ($message) use ($uuid){
//            $message->to('sedi.dev@gmail.com')
//                ->subject('Offer for agency agreement')
//                ->attach(storage_path('app/public/applications/offers/'.$uuid.'.pdf'), ['as' => "agreement.pdf"]);
//        });
    }
}
