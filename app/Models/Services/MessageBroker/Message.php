<?php


namespace App\Models\Services\MessageBroker;


use PhpAmqpLib\Wire\AMQPTable;

/**
 * Class Message
 * @package App\Models\Services\MessageBroker
 */
class Message
{
    /** @var array  */
    protected $body = [];

    /** @var AMQPTable */
    protected $headers;

    /**
     * @return array
     */
    public function getBody(): array
    {
        return $this->body;
    }

    /**
     * @param array $body
     */
    public function setBody(array $body): void
    {
        $this->body = $body;
    }

    /**
     * @return AMQPTable
     */
    public function getHeaders(): AMQPTable
    {
        return $this->headers;
    }

    /**
     * @param AMQPTable $headers
     */
    public function setHeaders(AMQPTable $headers): void
    {
        $this->headers = $headers;
    }
}
