<?php

namespace App\Models\Services\Auth;

use App\Traits\APIResponse;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthService
{
    use APIResponse;

    public function register($data)
    {
        $role = [
            'is_partner' => false,
            'is_manager' => false,
        ];

        if ($data['type']=='partner') {
            $role['is_partner'] = true;
            $data['is_active'] = false;
        }

        if ($data['type']=='manager') {
            $role['is_manager'] = true;
            $data['is_active'] = true;
        }

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'is_partner' => $role['is_partner'],
            'is_manager' => $role['is_manager'],
            'is_active' => $data['is_active']
        ]);

        if ($user) {
            return $this->sendResponse('', 200, true);
        }

        return $this->sendResponse('', 400, false);
    }

    public function login($data)
    {
        if (Auth::attempt($data)) {
            $user_type = (Auth::user()->is_partner) ? 'partner' : 'manager';
            $token = Auth::user()->createToken($data['email'])->accessToken;
            return $this->sendResponse('', 200, true, ['token' => $token, 'user_type' => $user_type]);
        }

        return $this->sendResponse('', 400, false);
    }
}
