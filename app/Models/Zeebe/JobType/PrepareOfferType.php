<?php


namespace App\Models\Zeebe\JobType;


use App\Models\Application;
use App\Models\DTO\TypeOperationDTO;
use App\Models\Handlers\CreateJobIfNotExists;
use App\Models\Zeebe\Job;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Storage;

class PrepareOfferType extends TypeAbstract
{
    public function operation(): void
    {
        /** @var TypeOperationDTO $typeOperation */
        $typeOperation = $this->property;
        var_dump((array) $this->property);

        (new CreateJobIfNotExists())->handle($typeOperation);

        /** @var Application $application */
        $application = Application::where('uuid', $typeOperation->getVariables()['uuid'])->firstOrFail();
        $pdf = PDF::loadView('diplomaOffer', [
            'company' => $application->company,
            'directorName' => $application->partner->name,
            'minCashback' => $application->min_cashback,
            'maxCashback' => $application->max_cashback,
            'commission' => $application->commission,
            'paymentFrequency' => $application->payment_period,
            'discount' => $application->discount
        ]);

        Storage::disk('public')->put('applications/offers/' . $application->uuid . '.pdf', $pdf->output());

//        $typeOperation->putToVariables('state', 'offer');

        $this->completeJob($typeOperation);
    }
}
