<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->index();
            $table->unsignedSmallInteger('state')->default(1);
            $table->boolean('is_rejected')->default(false);
            $table->unsignedBigInteger('partner_id');
            $table->foreign('partner_id')->references('id')->on('users');
            $table->unsignedBigInteger('manager_id')->nullable();
            $table->foreign('manager_id')->references('id')->on('users');
            //new
            $table->string('bin')->nullable();
            $table->string('company');
            $table->string('address')->nullable();
            //meeting
            $table->dateTime('meeting')->nullable();
            //offer
            $table->string('bank')->nullable();
            $table->string('payment_period')->nullable();
            $table->unsignedSmallInteger('commission')->default(0);
            $table->unsignedSmallInteger('min_cashback')->default(0);
            $table->unsignedSmallInteger('max_cashback')->default(0);
            $table->unsignedSmallInteger('discount')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
