<?php


namespace App\Models\Zeebe\JobType;


use App\Models\DTO\DTOInterface;

/**
 * Class TypeFactory
 * @package App\Models\Zeebe\JobType
 */
class TypeFactory
{
    /**
     * @param string $type
     * @param DTOInterface $dto
     * @return TypeAbstract
     * @throws \Exception
     */
    public function handle(string $type, DTOInterface $dto): TypeAbstract
    {
        switch ($type) {
            case TypeAbstract::TYPE_INITIALIZATION:
                return new InitType($dto);
            case TypeAbstract::TYPE_REVIEW_APP:
                return new ReviewApplicationType($dto);
            case TypeAbstract::TYPE_MEETING:
                return new MeetingType($dto);
            case TypeAbstract::TYPE_PREPARE_OFFER:
                return new PrepareOfferType($dto);
            case TypeAbstract::TYPE_RETRY_OFFER_SUBMIT:
                return new RetryOfferSubmitType($dto);
            case TypeAbstract::TYPE_OFFER_RESPONSE:
                return new OfferResponseType($dto);
            case TypeAbstract::TYPE_CALL_PARTNER_OFFER:
                return new CallPartnerOfferType($dto);
            case TypeAbstract::TYPE_OFFER_CALL_RESPONSE:
                return new OfferCallResponseType($dto);
            case TypeAbstract::TYPE_CREATE_ACC:
                return new CreateAccountType($dto);
            case TypeAbstract::TYPE_RETRY_ACCOUNT_SUBMIT:
                return new RetryAccountSubmitType($dto);
            case TypeAbstract::TYPE_ACC_RESPONSE:
                return new AccountResponseType($dto);
            case TypeAbstract::TYPE_CALL_PARTNER_ACC:
                return new CallPartnerAccountType($dto);
            case TypeAbstract::TYPE_ACC_CALL_RESPONSE:
                return new AccountCallResponseType($dto);
            case TypeAbstract::TYPE_REJECT_APP:
                return new RejectApplicationType($dto);
            case TypeAbstract::TYPE_MESSAGING:
                return new MessageSendType($dto);
            default:
                throw new \Exception('Type Not Implemented');
        }
    }
}
