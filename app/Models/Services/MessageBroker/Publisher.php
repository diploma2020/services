<?php


namespace App\Models\Services\MessageBroker;

use Amqp;
use Bschmitt\Amqp\Message as AMQPMessage;

/**
 * Class Publisher
 * @package App\Models\Services\MessageBroker
 */
abstract class Publisher implements PublisherInterface, EventSourceInterface
{
    /**
     * @return string
     */
    public abstract function getExchange(): string;

    /**
     * @return string
     */
    public abstract function getRouting(): string;

    /**
     * @param Message $message
     */
    public function publish(Message $message): void
    {
        $properties = [
            'application_headers' => $message->getHeaders(),
            'content_type' => 'application/json',
        ];

        $amqpMessage = new AMQPMessage(json_encode($message->getBody()), $properties);

        Amqp::publish(
            $this->getRouting(),
            $amqpMessage,
            ['exchange' => $this->getExchange()]
        );
    }
}
