<?php


namespace App\Models\Services\MessageBroker;


interface SubscriberInterface
{
    public function subscribe(): void;
}
