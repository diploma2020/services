<?php


namespace App\Models\Services\Mail;


use App\Models\Application;
use Illuminate\Support\Facades\Mail;

class AccountInfoSend
{
    public static function submit(string $uuid)
    {
        $to = Application::getByUuidOrFail($uuid)->partner->email;
        Mail::to('sedi.dev@gmail.com')
            ->send(new \App\Mail\AccountInfoSend());
    }
}
